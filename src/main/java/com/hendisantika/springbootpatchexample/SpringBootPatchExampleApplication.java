package com.hendisantika.springbootpatchexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class SpringBootPatchExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPatchExampleApplication.class, args);
	}
}
