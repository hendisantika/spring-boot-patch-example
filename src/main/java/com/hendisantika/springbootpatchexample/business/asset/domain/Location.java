package com.hendisantika.springbootpatchexample.business.asset.domain;

import lombok.Data;

import javax.persistence.Entity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/02/18
 * Time: 06.52
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Data
public class Location extends BasicEntity {

    private String name;
}
