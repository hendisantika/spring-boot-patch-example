package com.hendisantika.springbootpatchexample.business.asset.service;

import com.hendisantika.springbootpatchexample.business.asset.domain.Asset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/02/18
 * Time: 21.09
 * To change this template use File | Settings | File Templates.
 */

@Service
@Transactional
public class AssetServiceImpl implements AssetService {

    @Autowired
    AssetRepository assetRepository;

    @Override
    public List<Asset> findAll() {
        return assetRepository.findAll();
    }

    @Override
    public Asset find(String id) {
        return assetRepository.findOne(id);
    }

    @Override
    public Asset save(Asset asset) {
        return assetRepository.save(asset);
    }

    @Override
    public void delete(String id) {
        assetRepository.delete(id);
    }
}
