package com.hendisantika.springbootpatchexample.business.asset.service;

import com.hendisantika.springbootpatchexample.business.asset.domain.Location;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/02/18
 * Time: 21.10
 * To change this template use File | Settings | File Templates.
 */
public interface LocationService {
    List<Location> findAll();
}
