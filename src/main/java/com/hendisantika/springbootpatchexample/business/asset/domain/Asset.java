package com.hendisantika.springbootpatchexample.business.asset.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.time.ZonedDateTime;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/02/18
 * Time: 06.52
 * To change this template use File | Settings | File Templates.
 */

@Data
@Entity
public class Asset extends BasicEntity {

    private String name;
    private ZonedDateTime dateAcquired;
    private long count;

    @ManyToMany
    @JoinTable(name = "asset_location",
            joinColumns = @JoinColumn(name = "asset_id"),
            inverseJoinColumns = @JoinColumn(name = "location_id"))
    private Set<Location> locations;
}