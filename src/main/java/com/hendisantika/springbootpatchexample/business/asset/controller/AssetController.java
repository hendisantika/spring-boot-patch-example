package com.hendisantika.springbootpatchexample.business.asset.controller;

import com.hendisantika.springbootpatchexample.business.asset.domain.Asset;
import com.hendisantika.springbootpatchexample.business.asset.service.AssetService;
import com.hendisantika.springbootpatchexample.config.patch.json.Patch;
import com.hendisantika.springbootpatchexample.config.patch.json.PatchRequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/02/18
 * Time: 15.08
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/api/v1/assets")
public class AssetController {
    @Autowired
    AssetService assetService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Asset> list() {

        return assetService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Asset create(@RequestBody Asset asset) {

        return assetService.save(asset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Asset update(@PathVariable String id,
                        @RequestBody Asset asset) {

        asset.setId(id);
        return assetService.save(asset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @Patch(service = AssetService.class, id = String.class)
    public Asset patch(@PathVariable String id,
                       @PatchRequestBody Asset asset) {

        asset.setId(id);
        return assetService.save(asset);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {

        assetService.delete(id);
    }
}
