package com.hendisantika.springbootpatchexample.business.asset.controller;

import com.hendisantika.springbootpatchexample.business.asset.domain.Location;
import com.hendisantika.springbootpatchexample.business.asset.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/02/18
 * Time: 15.09
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/api/v1/locations")
public class LocationController {
    @Autowired
    LocationService locationService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Location> list() {

        return locationService.findAll();
    }
}
