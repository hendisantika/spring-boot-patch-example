package com.hendisantika.springbootpatchexample.business.asset.service;

import com.hendisantika.springbootpatchexample.business.asset.domain.Asset;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/02/18
 * Time: 21.07
 * To change this template use File | Settings | File Templates.
 */
public interface AssetRepository extends JpaRepository<Asset, String> {
}
