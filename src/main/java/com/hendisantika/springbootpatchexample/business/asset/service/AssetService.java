package com.hendisantika.springbootpatchexample.business.asset.service;

import com.hendisantika.springbootpatchexample.business.asset.domain.Asset;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/02/18
 * Time: 21.08
 * To change this template use File | Settings | File Templates.
 */
public interface AssetService {
    List<Asset> findAll();

    Asset find(String id);

    Asset save(Asset asset);

    void delete(String id);
}
