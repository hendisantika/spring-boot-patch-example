package com.hendisantika.springbootpatchexample.config.patch.json;

import java.lang.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-patch-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/02/18
 * Time: 07.49
 * To change this template use File | Settings | File Templates.
 */

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PatchRequestBody {
}
